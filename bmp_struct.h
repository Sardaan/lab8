#ifndef _BMP_STRUCT_H___
#define _BMP_STRUCT_H___

struct __attribute__((packed)) bmp_header
{
    uint16_t type;
    uint32_t file_size; 
    uint32_t reserved;
    uint32_t data_start_addr;
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t color_planes_num;
    uint16_t bits_per_pixel;
    uint32_t compression_method;
    uint32_t image_size;
    uint32_t x_pixels_per_meter;
    uint32_t y_pixels_per_meter;
    uint32_t colors_num;
    uint32_t important_colors_num;
};

typedef struct pixel {
    uint8_t r, g, b, a;
} pixel;

struct __attribute__((packed)) bmp_pixel_bgr {
    uint8_t b, g, r;
};

typedef struct point {
    uint32_t x, y;
} point;

typedef struct image image;

struct image {
    uint32_t width;
    uint32_t height;
    pixel* data_array;
};

#endif