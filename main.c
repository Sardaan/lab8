#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include "bmp.h"
#include "bmp_struct.h"

void print_error(error status);

int main(int arg_num, char** args)
{


	char* src_file = args[1];
    char* dst_file_c = args[2];
    char* dst_file_asm = args[3];

    image* image;
    struct image* img_asm;

    error status = image_read_from_bmp(src_file, &image);
    image_read_from_bmp(src_file, &img_asm);


    if (status == OK) {
        
        struct rusage r;
        struct timeval start;
        struct timeval end;
        getrusage(RUSAGE_SELF, &r);
        start = r.ru_utime;
        image_make_sepia_c(image);

        getrusage(RUSAGE_SELF, &r);
        end = r.ru_utime;
        long res = end.tv_usec - start.tv_usec;
        printf( "Время выполнения sepia (с): %ld\n", res);
        printf( "Начало: %d\n", start.tv_usec);
        printf( "Конец: %d\n", end.tv_usec);
        error save_status1 = image_save_to_bmp(dst_file_c, image);


        getrusage(RUSAGE_SELF, &r);
        start = r.ru_utime;
        image_make_sepia_asm(img_asm);
        getrusage(RUSAGE_SELF, &r);
        end = r.ru_utime;
        res = end.tv_usec - start.tv_usec;
        printf( "Время выполнения sepia (asm): %ld\n", res);
        printf( "Начало: %d\n", start.tv_usec);
        printf( "Конец: %d\n", end.tv_usec);
        error save_status2 = image_save_to_bmp(dst_file_asm, img_asm);

        if (save_status1 == OK && save_status2 == OK) {
            puts("Готово!");
        } else { 
            print_error(save_status1); 
            print_error(save_status2); 
        }
    } else { 
        print_error(status); 
    }

    return 0;
}

void print_error(error status) {
    if (status == FILE_ERR)
        puts("File error");
    else if (status == NOT_BMP_ERR)
        puts("wrong bmp format");
    else
        puts("Noooo");
}
