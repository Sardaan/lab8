#ifndef _BMP_H___
#define _BMP_H___
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#include "bmp_struct.h"

#define MAGIC 0x4d42

typedef enum error{
    OK,
    FILE_ERR,
    NOT_BMP_ERR
}error;

error image_read_from_bmp(char* bmp_file_name, image** dst);

error image_save_to_bmp(char* bmp_file_name, image* src);

// void rotate(image* image, double degrees);
image* image_create(uint32_t width, uint32_t height);
image* image_copy(image* image);
uint32_t image_get_width(image* image);
uint32_t image_get_height(image* image);
pixel image_get_pixel(image* image, point point);
bool image_set_pixel(image* image, point point, pixel pixel);
bool image_check_bounds(image* image, point point);
void image_make_sepia_c(image* image);
void image_make_sepia_asm(image* image);
pixel image_get_pixel_by_idx(image* image, size_t idx);
void image_set_pixel_by_idx(image* image, size_t idx, pixel p);



#endif
