C_SRC_K = -g -c

all:
	gcc $(C_SRC_K) bmp.c -o bmp.o
	nasm -felf64 -g sepia.asm -o sepia.o
	gcc $(C_SRC_K) main.c -o main.o
	gcc -g *.o -o main
	
clean:
	rm main *.o

run1: main
	./main tests/1.bmp tests/1_c.bmp tests/1_asm.bmp

runF: main
	./main tests/image1.bmp tests/3_c.bmp tests/3_asm.bmp 
